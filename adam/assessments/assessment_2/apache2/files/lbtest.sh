#!/bin/bash
# NOTE: You need the template file /files/testtemplate
# in order for the test page to display

# VARIABLES
# searches for ip address of current webserver
MY_IP_ADDR=$(ifconfig | grep 192 | awk '{print $2}')
MY_OS=$(hostnamectl | grep 'Operating System' | sed -e 's/Operating System: //g' | sed 's/^ *//;s/ *$//')
MY_OS_KERNEL=$(hostnamectl | grep 'Kernel: ' | sed -e 's/Kernel: //g' | sed 's/^ *//;s/ *$//')
MY_WEBSERVER_SOFTWARE=$1

# searches for ip address of current webserver
if [ "$1" == "apache" ];
  then
    WRITEPATH="/var/www/html"
    MY_WEBSERVER_VERSION=$(httpd -v | grep version |  sed 's/Server version: Apache\///' | sed 's/ (CentOS)//')
elif [ "$1" == "nginx" ];
  then
    WRITEPATH="/usr/share/nginx/html"
    MY_WEBSERVER_VERSION=$(nginx -v 2>&1 | sed 's/nginx version: nginx\///')
elif [ "$1" == "wildfly"];§§
  then
    echo "please supply the lbtest.sh file with an appropriate filepath for wildfly"
    # WRITEPATH="<insert writepath here"
else
  echo "Please enter a valid webserver, nginx and apache currently supported."
  echo "SYNTAX: /path/lbtest.sh <webserver name>"
  echo "webserver name options: nginx, apache, wildfly"
  exit 1
fi

# SET UP
# writes webpage
# replaces MY_IP_ADDR with correct value in webpage file
# writes index.html file into the web server

cat >/vagrant/files/webpage <<_END_
<html>
<p>THIS IS A LOAD BALANCER TEST PAGE</p>
<p>My IP address is $MY_IP_ADDR</p>
<p>I'm on $MY_OS operating system with kernel $MY_OS_KERNEL</p>
<p>I'm running $MY_WEBSERVER_SOFTWARE version $MY_SOFTWARE_VERSION</P>

</html>
_END_

mv -f /vagrant/files/webpage $WRITEPATH/index.html
