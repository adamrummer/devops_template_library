#!/bin/bash
# this file tests load balancers to see if they are working
# it currently works on apache, nginx.

# VARIABLES
# searches for ip address of current webserver
MY_IP_ADDR=$(ifconfig | grep 192 | awk '{print $2}')
# searches for ip address of current webserver
if [ "$1" == "apache" ];
  then
    WRITEPATH="/var/www/html"
elif [ "$1" == "nginx" ];
  then
    WRITEPATH="/usr/share/nginx/html"
elif [ "$1" == "wildfly"];
  then
    echo "please supply the lbtest.sh file with an appropriate filepath for wildfly"
    # WRITEPATH="<insert writepath here"
else
  echo "Please enter a valid webserver, nginx and apache currently supported."
  echo "SYNTAX: /path/lbtest.sh <webserver name>"
  echo "webserver name options: nginx, apache, wildfly"
  exit 1
fi

# SET UP
# writes webpage
# replaces MY_IP_ADDR with correct value in webpage file
# writes index.html file into the web server

cat >/vagrant/files/webpage <<_END_
<html>
<p>THIS IS A LOAD BALANCER TEST PAGE</p>
<p>My IP address is MY_IP_ADDR</p>
<p>You're on MY_OS operating system (currently not working)</p>
</html>
_END_

sed -i "s/MY_IP_ADDR/$MY_IP_ADDR/g" /vagrant/files/webpage
mv -f /vagrant/files/webpage $WRITEPATH/index.html
