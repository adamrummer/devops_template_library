#!/bin/bash
# NOTE: You need the template file /files/testtemplate
# in order for the test page to display

# VARIABLES
# searches for ip address of current webserver
MY_IP_ADDR=$(ifconfig | grep 192 | awk '{print $2}')
# sets correct path based on webserver argument
if [ "$1" == "apache" ];
  then
    WRITEPATH="/var/www/html"
elif [ "$1" == "nginx" ];
  then
    WRITEPATH="/usr/share/nginx/html"
elif [ "$1" == "wildfly"];
  then
    echo "please supply the lbtest.sh file with an appropriate filepath for wildfly"
    # WRITEPATH="<insert writepath here"
else
  echo "Please enter a valid webserver, nginx and apache currently supported."
  echo "SYNTAX: /path/lbtest.sh <webserver name>"
  echo "webserver name options: nginx, apache, wildfly"
  exit 1
fi

# SET UP
# creates clean webpage file for each new machine
# CODE
# replaces MY_IP_ADDR with correct value in webpage file
# writes index.html file into the web server
cp /vagrant/test/testtemplate /vagrant/files/webpage
sed -i "s/MY_IP_ADDR/$MY_IP_ADDR/g" /vagrant/files/webpage
mv -f /vagrant/files/webpage $WRITEPATH/index.html
