d1 = {'name':'Adam', 'employer':'AL'}
d2 = {'name':'spoon', 'species':'cat', 'colour':'none'}

for thing in [d1, d2]:
    print 'This thing is called: {name}'.format(name=thing['name'])
    try:
        print 'The employer of {name} is {employer}'.format(name=thing['name'], employer=thing['employer'])
        print 0/0
    except ZeroDivisionError:
        print "You can't divide by zero."
    except KeyError:
        print 'Thing called {name} does not have an employer'.format(name=thing['name'])

try:
    a = 7/fish
except NameError:
    print 'hahahahaa fish'
