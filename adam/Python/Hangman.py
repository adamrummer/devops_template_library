# Hangman game which uses the inbuilt python library

# Imports random package
import random

# Sets variables
guesslist = []
wordlist = []
i = 0
guesses = 0
wrongguesses = 0

# Gets word to use for game
with open('/usr/share/dict/words') as file_object:
    dictionary = file_object.readlines()
while True:
    word = dictionary[random.randint(1, len(dictionary))]
    if word.islower() and len(word) > 4:
        break


# prints asterisks to represent word
print "Here's your word."
asteriskslist = []

for i in range(len(word.strip())):
    asteriskslist.append('*')
print ''.join(asteriskslist)

# Sets guess limit and end game condition


for letter in word.strip():
    wordlist.append(letter)
    # print "DBG1: wordlist is {wl}".format(wl=wordlist)

while wrongguesses < 8:

    if '*' not in asteriskslist:
        print 'You win! you made %s guesses' % str(guesses)
        exit

    # block of code for guessing
    guess = raw_input('Your guess:')
    guesses += 1
    guesslist.append(guess)
    print 'Your guesses:', sorted(guesslist)

    for i in range(len(wordlist)):
        if guess == wordlist[i]:
            asteriskslist[i] = guess
            # print "DBG2: replacing character {c} in
            # {asterisks}".format(c=i, asterisks=asteriskslist)

    if guess in word:
        print 'letter %s is in the word.' % guess
    else:
        print 'letter %s is not in the word.' % guess
        wrongguesses += 1

    print ''.join(asteriskslist)
    print '---------------------------------'

print 'Too many guesses. You lose! \n The word was ', word
exit
