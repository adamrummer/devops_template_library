
# self argument stops Python from deleting the variables after the function is called.
# to delete an object use: del object_name
# -------------------------------------------------------
# Defining classes

class Person():
    """
        A class that defines a person
    """
    def __init__(self, myname):
        self.name = myname

    def greet(self):
        """Introduce myself, mentioning my name"""
        print "Hello. I am {me}.".format(me=self.name)
class Singer(Person):
    def sing(self):
        print 'tra la laaaaaaaaaaa!'
class Operasinger(Singer):
    pass
class Computer():
    def reboot(self):
        pass
c = Computer()
c.reboot()
# -------------------------------------------------------
# Doing stuff
x = Person('Cheryl')
y = Singer('Alex')
z = Operasinger('Justin')
for item in [x,y,z]:
    # print "Item {item} is of type {t}".format(item=item, t=type(item))
    if isinstance(item, Person):
        item.greet()
    if isinstance(item, Singer):
        item.sing()

help(Singer)
