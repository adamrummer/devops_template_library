# This program prints in the terminal all lines in the text
# file that are shorter than 20 lines

with open('text.txt') as file_object:
    for line in file_object.readlines():
        line=line.strip()
        if 0 < len(line) < 20:
            print line
exit
