#!/bin/bash
# This script creates an amazon database instance and populates it

# Creates Database in AWS
instance_identifier = 'thebeststore'

aws rds create-db-instance \
--db-instance-identifier $instance_identifier \
--db-instance-class db.t2.medium \
--engine mariaDB \
--master-username adam \
--master-user-password secretsecret \
--allocated-storage 8192 \
--db-subnet-group-name try1 \
--publicly-accessible \

# Waits for database to boot up and continues script once it is available
state = ''
while [ $state != 'available' ]
do
  state = $(aws rds describe-db-instances --db-instance-identifier $instance_identifier --query 'DBinstances[0].DBinstancestatus' )
  sleep 10
done

# Specifies the address of the end point, automatically grabbing it from AWS
# Specifies the SQL script to provision the database / create the database schema
endpoint = $(aws rds describe-db-instances --db-instance-identifier $instance_identifier --query 'DBinstances[0].Endpoint.Address')
sql_script = ../mysql/files/mydb

# Runs mydb file, to create users and populate database
mysql -h $endpoint -u adam -psecretsecret <$sql_script
